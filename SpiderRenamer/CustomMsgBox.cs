﻿using System.Drawing;
using System.Windows.Forms;

namespace SpiderRenamer
{
    public partial class CustomMsgBox : Form
    {
        public CustomMsgBox(string msg, Image icn)
        {
            InitializeComponent();
            message.Text = msg;
            icon.Image = icn;

        }

        private void OK_Click(object sender, System.EventArgs e)
        {
            ActiveForm?.Close();
        }
    }
}
