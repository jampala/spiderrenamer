﻿using SpiderRenamer.Service.Impl;

namespace SpiderRenamer.Service
{
    public interface IRenamingService
    {
        RenameResponse Rename(string traineeFirstName, string billDate, string sourcePath, string destinationPath);
    }
}
