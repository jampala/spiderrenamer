﻿namespace SpiderRenamer.Service.Impl
{
     public class RenameResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
