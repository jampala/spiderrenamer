﻿using System;
using System.IO;
using log4net;
using log4net.Config;

namespace SpiderRenamer.Service.Impl
{
    public class RenamingService : IRenamingService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(Program));

        public RenameResponse Rename(string traineeFirstName, string billDate, string sourcePath, string destinationPath)
        {
            var suffixName = traineeFirstName + "_" + billDate;
            var tempPath = Path.Combine(destinationPath, "_temp");
            var tempDirectory = new DirectoryInfo(tempPath);
            var srcDirectory=new DirectoryInfo(sourcePath);
            RenameResponse result;
            BasicConfigurator.Configure();

            try
            {
                CleanUpTempDirectory(tempPath);
                CreateTempDirectory(destinationPath);
                MoveToTempDirectory(srcDirectory,sourcePath, tempPath);
                MoveFilesToDestinationDirectory(tempDirectory, suffixName, destinationPath);
                result = new RenameResponse {Success = true, Message = "Rename Done"};
            }
            catch (Exception ex)
            {
                result = new RenameResponse
                {
                    Success = false,
                    Message = "an error occurred: This may be because file with the same name already exists"
                };
                _log.Error(result, ex);
            }
            finally
            {
                CleanUpTempDirectory(tempPath);
            }

            return result;
        }

        private void MoveToTempDirectory(DirectoryInfo source,string sourcePath, string tempDirectoryPath)
        {
            var files = source.GetFiles();
            foreach (var file in files)
            {
                //file.CopyTo(tempDirectoryPath);
                File.Copy(sourcePath+file.Name,tempDirectoryPath+"/"+file.Name);

            }
        }

        private void CreateTempDirectory(string destinationPath)
        {
            var destdir = new DirectoryInfo(destinationPath);
            var tempDirectory = Path.Combine(destinationPath, "_temp");
            if (!Directory.Exists(tempDirectory))
                destdir.CreateSubdirectory("_temp");
        }

        private string GetNewFileName(string fileName, string suffixName)
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var extension = Path.GetExtension(fileName);
            return string.Format("{0}_{1}{2}", fileNameWithoutExtension, suffixName, extension);
            ;
        }

        private void MoveFilesToDestinationDirectory(DirectoryInfo tempDirectory, string suffixName,
            string destinationPath)
        {
            var files = tempDirectory.GetFiles();
            foreach (var file in files)
            {
                var newFileName = GetNewFileName(file.Name, suffixName);
                // Create the path to the new copy of the file.

                var renamePath = Path.Combine(destinationPath, newFileName);
                // Rename the file
                file.MoveTo(renamePath);
            }
        }

        private void CleanUpTempDirectory(string tempDirectoryPath)
        {
            var tempDirectory=new DirectoryInfo(tempDirectoryPath);
            if (tempDirectory.Exists)
            {
                foreach (FileInfo file in tempDirectory.GetFiles())
                {
                    file.Delete();
                }
                tempDirectory.Delete();
            }
            
        }
    }
}
