﻿namespace SpiderRenamer
{
    partial class SpiderRenamerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpiderRenamerForm));
            this.label1 = new System.Windows.Forms.Label();
            this.traineeFirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sourcePath = new System.Windows.Forms.TextBox();
            this.destinationPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.traineeBillDate = new System.Windows.Forms.DateTimePicker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider3 = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProvider4 = new System.Windows.Forms.ErrorProvider(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.WarningForPaths = new System.Windows.Forms.Label();
            this.WarningForName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TraineeFirstName :";
            // 
            // traineeFirstName
            // 
            this.traineeFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.traineeFirstName.Location = new System.Drawing.Point(260, 87);
            this.traineeFirstName.Name = "traineeFirstName";
            this.traineeFirstName.Size = new System.Drawing.Size(244, 20);
            this.traineeFirstName.TabIndex = 1;
            this.traineeFirstName.Validating += new System.ComponentModel.CancelEventHandler(this.traineeFirstName_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "BillDate :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "SourcePath :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(71, 263);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "DestinationPath :";
            // 
            // sourcePath
            // 
            this.sourcePath.Location = new System.Drawing.Point(260, 218);
            this.sourcePath.Name = "sourcePath";
            this.sourcePath.Size = new System.Drawing.Size(339, 20);
            this.sourcePath.TabIndex = 6;
            this.sourcePath.Validating += new System.ComponentModel.CancelEventHandler(this.sourcePath_Validating);
            // 
            // destinationPath
            // 
            this.destinationPath.Location = new System.Drawing.Point(260, 260);
            this.destinationPath.Name = "destinationPath";
            this.destinationPath.Size = new System.Drawing.Size(339, 20);
            this.destinationPath.TabIndex = 7;
            this.destinationPath.TextChanged += new System.EventHandler(this.destinationPath_TextChanged);
            this.destinationPath.Validating += new System.ComponentModel.CancelEventHandler(this.destinationPath_Validating);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(263, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Rename";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.Rename);
            // 
            // traineeBillDate
            // 
            this.traineeBillDate.Checked = false;
            this.traineeBillDate.CustomFormat = "dd-MM-yyyy";
            this.traineeBillDate.Location = new System.Drawing.Point(260, 130);
            this.traineeBillDate.Name = "traineeBillDate";
            this.traineeBillDate.Size = new System.Drawing.Size(244, 20);
            this.traineeBillDate.TabIndex = 9;
            this.traineeBillDate.ValueChanged += new System.EventHandler(this.traineeBillDate_ValueChanged);
            this.traineeBillDate.Validating += new System.ComponentModel.CancelEventHandler(this.billDate_Validating);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // errorProvider2
            // 
            this.errorProvider2.ContainerControl = this;
            // 
            // errorProvider3
            // 
            this.errorProvider3.ContainerControl = this;
            // 
            // errorProvider4
            // 
            this.errorProvider4.ContainerControl = this;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(605, 216);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Browse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.BrowseSourcePath);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(605, 257);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Browse";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.BrowseDestinationPath);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(71, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Resulting File Name Example:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(260, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Trainee_01-05-2017_GFS_Bill";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(71, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Instructions: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(257, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(490, 30);
            this.label8.TabIndex = 15;
            this.label8.Text = " Place all training files to be renamed in one folder.  \r\n Complete the below fie" +
    "ld entries including the selection of the files upon clicking Browse.";
            // 
            // WarningForPaths
            // 
            this.WarningForPaths.AutoSize = true;
            this.WarningForPaths.ForeColor = System.Drawing.Color.Red;
            this.WarningForPaths.Location = new System.Drawing.Point(260, 283);
            this.WarningForPaths.Name = "WarningForPaths";
            this.WarningForPaths.Size = new System.Drawing.Size(13, 13);
            this.WarningForPaths.TabIndex = 16;
            this.WarningForPaths.Text = "  ";
            // 
            // WarningForName
            // 
            this.WarningForName.AutoSize = true;
            this.WarningForName.ForeColor = System.Drawing.Color.Red;
            this.WarningForName.Location = new System.Drawing.Point(531, 90);
            this.WarningForName.Name = "WarningForName";
            this.WarningForName.Size = new System.Drawing.Size(13, 13);
            this.WarningForName.TabIndex = 17;
            this.WarningForName.Text = "  ";
            // 
            // SpiderRenamerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 359);
            this.Controls.Add(this.WarningForName);
            this.Controls.Add(this.WarningForPaths);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.traineeBillDate);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.destinationPath);
            this.Controls.Add(this.sourcePath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.traineeFirstName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SpiderRenamerForm";
            this.Text = "FAO Training – File Naming Utility";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox traineeFirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox sourcePath;
        private System.Windows.Forms.TextBox destinationPath;
        private System.Windows.Forms.DateTimePicker traineeBillDate;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.ErrorProvider errorProvider2;
        private System.Windows.Forms.ErrorProvider errorProvider3;
        private System.Windows.Forms.ErrorProvider errorProvider4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label WarningForPaths;
        private System.Windows.Forms.Label WarningForName;
    }
}

