﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SpiderRenamer.Properties;
using SpiderRenamer.Service.Impl;

namespace SpiderRenamer
{
    public partial class SpiderRenamerForm : Form
    {
        public SpiderRenamerForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            traineeBillDate.Format = DateTimePickerFormat.Custom;
            traineeBillDate.CustomFormat = @"dd-MM-yyyy";
        }

        private void BrowseSourcePath(object sender, EventArgs e)
        {
            var folderBrowserDialog = new FolderBrowserDialog();

            folderBrowserDialog.ShowDialog();

            if (!string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                sourcePath.Text = $@"{Path.GetFullPath(folderBrowserDialog.SelectedPath)}/{""}";
        }

        private void BrowseDestinationPath(object sender, EventArgs e)
        {
            var folderBrowserDialog = new FolderBrowserDialog();

            folderBrowserDialog.ShowDialog();

            if (!string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                destinationPath.Text = $@"{Path.GetFullPath(folderBrowserDialog.SelectedPath)}/{""}";
        }


        private void Rename(object sender, EventArgs e)
        {
            if (sourcePath.Text != destinationPath.Text)
            {
                var renameService = new RenamingService();
                var result = renameService.Rename(traineeFirstName.Text, traineeBillDate.Text, sourcePath.Text,
                    destinationPath.Text);

                var renamedFile = traineeFirstName.Text + "_" + traineeBillDate.Text;
                if (result.Success)
                {
                    var msgBox =
                        new CustomMsgBox(
                            @"Renamed files with suffix- " + renamedFile.TrimEnd() + @" and" + Environment.NewLine +
                            @"Files are located at: " +
                            destinationPath.Text.TrimEnd(destinationPath.Text[destinationPath.Text.Length - 1]),
                            Resources.Ok_icon);
                    msgBox.ShowDialog(this);
                }
                else
                {
                    var msgBox =
                        new CustomMsgBox(
                            @"Renaming files with suffix- " + renamedFile.TrimEnd() + @" is failed " +
                            Environment.NewLine +
                            result.Message, Resources.Close_2_icon);
                    msgBox.ShowDialog(this);
                }
                WarningForPaths.Text = "";
            }
            else
            {
                WarningForPaths.Text = @"Source and destination paths should not be same";
            }
        }

        /// <summary>
        ///     Validations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void traineeFirstName_Validating(object sender, CancelEventArgs e)
        {
            var notAllowedChars = "#><$+%@:!`&*{}'/?";
            if (traineeFirstName.Text != "")
            {
                errorProvider1.Clear();
                
            }
            else
            {
                errorProvider1.SetError(traineeFirstName, "Required");

            }
            if(traineeFirstName.Text.All(notAllowedChars.Contains))
            {
                WarningForName.Text = @"Please avoid special characters";
            }
            else
            {
                WarningForName.Text = "";
            }

            
        }

        private void billDate_Validating(object sender, CancelEventArgs e)
        {
            if (traineeBillDate.Text != "")
            {
                errorProvider2.Clear();
            }
            else
            {
                errorProvider2.SetError(traineeBillDate, "Required");
            }
        }

        private void sourcePath_Validating(object sender, CancelEventArgs e)
        {
            if (sourcePath.Text != "")
            {
                errorProvider3.Clear();
            }
            else
            {
                errorProvider3.SetIconPadding(sourcePath, -20);
                errorProvider3.SetError(sourcePath, "Required");
            }
        }

        private void destinationPath_Validating(object sender, CancelEventArgs e)
        {
            if (destinationPath.Text != "")
            {
                errorProvider4.Clear();
                
            }
            else
            {
                errorProvider4.SetIconPadding(destinationPath, -20);
                errorProvider4.SetError(destinationPath, "Required");
            }      

        }

        private void traineeBillDate_ValueChanged(object sender, EventArgs e)
        {
        }

        private void destinationPath_TextChanged(object sender, EventArgs e)
        {
            button1.Visible = true;
        }
    }
}